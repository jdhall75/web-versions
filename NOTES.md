# a web service to track versions of files

## Schema

```sql
CREATE TABLE hosts (
    id int not null autoincrement,
    hostnames VARCHAR(255) not null,
    head VARCHAR(255),
    created timestamp,
    updated timestamp,
    PRIMARY KEY(id)
)

CREATE TABLE hostdata (
    id int not null autoincrement,
    host_id int not null,
    data TEXT,
    created timestamp,
    PRIMARY KEY(id),
    FOREIGN KEY(host_id),
)
```

# create a sha-1 of the content

```python
# import the library module
import hashlib

# initialize a string
str = "data in the files to get a sha-1 of"

# encode the string
encoded_str = str.encode()

# create a sha1 hash object initialized with the encoded string
hash_obj = hashlib.sha1(encoded_str)

# convert the hash object to a hexadecimal value
hexa_value = hash_obj.hexdigest()

# print
print("\n", hexa_value, "\n")
```
