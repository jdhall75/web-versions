from fastapi import FastAPI

app = FastAPI()


@app.get("/")
def read_root():
    return {"msg": "hello, world"}


#
# posting the content of a file to the api keyed to the hostname
# sha-1 hash is produced for the content of the file
# compared to the hosts table head->sha1 hash
#